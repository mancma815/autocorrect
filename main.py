from pynput import keyboard
import unidecode

kb = keyboard.Controller()

current_word = ""
word_list = []

term_keys = [keyboard.Key.space]

with open('czech.tsv', "r", encoding="utf-8") as file:
    for row in file:
        word_list.append((row.split()[0]))

lookup_list = []

for word in word_list:
    lookup_list.append(unidecode.unidecode(word))


def on_press(key):
    global term_keys
    try:
        global current_word
        current_word += key.char
    except AttributeError:
        #print('special key {0} pressed'.format(key))
        if key == keyboard.Key.backspace:
            current_word = current_word[:-1]
        if key in term_keys:
            print(current_word)
            if current_word in lookup_list:
                accented_word = word_list[lookup_list.index(current_word)]
                if accented_word != current_word:
                    kb.press(keyboard.Key.ctrl)
                    kb.press(keyboard.Key.backspace)
                    kb.release(keyboard.Key.ctrl)
                    kb.release(keyboard.Key.backspace)
                    kb.type(accented_word+" ")
            current_word = ""

def on_release(key):
    if key == keyboard.Key.esc:
        print("pressed esc")
        # Stop listener
        return False

with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
